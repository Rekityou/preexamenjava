const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener("click", function () {
    const numTabla = document.getElementById("numTabla");
    const tablas = document.getElementById("txtTablas");
    const tablaSelec = parseInt(numTabla.value);
    tablas.innerHTML = '';

    for (x = 1; x <= 10;x++) {

        const res = tablaSelec * x;
        const celdaTabla = document.createElement("DIV");
        celdaTabla.classList.add("table-cell");

        const imgTabla = document.createElement("img");
        imgTabla.classList.add("digit");
        imgTabla.src = "/img/" + tablaSelec + ".png";
        celdaTabla.appendChild(imgTabla);

        const imgX = document.createElement("img");
        imgX.classList.add("digit");
        imgX.src = "/img/x.png";
        celdaTabla.appendChild(imgX);

        for (let digit of x.toString()) {
            const imgNum = document.createElement("img");
            imgNum.classList.add("digit");
            imgNum.src = '/img/' + digit + ".png";
            celdaTabla.appendChild(imgNum);
        }

        const imgIgual = document.createElement("img");
        imgIgual.classList.add("digit");
        imgIgual.src = "/img/=.png";
        celdaTabla.appendChild(imgIgual);

        for (let digit of res.toString()) {
            const imgNum = document.createElement("img");
            imgNum.classList.add("digit");
            imgNum.src = '/img/' + digit + ".png";
            celdaTabla.appendChild(imgNum);
        }
        tablas.appendChild(celdaTabla);
    }
});